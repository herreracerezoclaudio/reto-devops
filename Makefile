#Hacemos un makefile para subir y bajar las imagenes de dockery comprobar el funcionamiento
all: docker_down docker_up test_app deploy
docker_down:
	       	cd docker/ && docker-compose down && cd ..

docker_up:
	        cd docker/ && docker-compose up -d --build --force-recreate && cd ..

test_app:
	curl https://localhost:443
	curl https://localhost:443/public
	curl https://localhost:443/private
# Probamos ademas el trafico en el puerto 80 para comprobar que redirije al 443	
	curl http://localhost:80
	curl http://localhost:80/public
	curl http://localhost:80/private

# deploy k8s
deploy:
	cd kubernetes && kubectl -- apply -f kubernetes/deployment-k8s.yaml
